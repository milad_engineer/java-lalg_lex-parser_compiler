/* UNIVERSIDADE CATOLICA DOM BOSCO - CAMPO GRANDE MS
DISCIPLINA DE COMPILADORES II
TRABALHO FINAL DE COMPILADORES
NOME DO PROJETO: IMPLEMENTACAO DO ANALISADOR LEXICO E SINTATICO DA LINGUAGEM LALGOL
PROFESSOR: PERICLES CHRISTIAN MORAES LOPES
ALUNO-AUTOR: MILAD ROGHANIAN
DATA DE IMPLEMENTACAO: DE 04/2019 A 02/05/2019
*/

package compilador_lex_parser;
//Inicio em 20/09/2018

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException 
    {
        MaquinasdeEstadoSintatico sm = new MaquinasdeEstadoSintatico();
        
        sm.initCompilation();
    }
    
}
