package compilador_lex_parser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ControleSintatico
{
    ArrayList<String> lines = new ArrayList<String>();
    private String output = new String();
    private String[] vet_tokens;
    private Integer global_counter = 0;
    static final int LINE_TO_LINE = 0; //modo de leitura do lexico linha a linha
    static final int LINE_TO_END = 1; //modo de leitura do sintatico da linha ate o fim
    
    public void initParsing() throws IOException{
        ControleLexico con = new ControleLexico();
        
        //realiza compilacao lexica
        con.readtxt(); //le o texto de entrada
        con.printImput(); //printa o codigo de entrada
        con.compileLex(LINE_TO_END,this.global_counter); //compila todas as linhas do codigo
        con.insertEndLex(); //insere o caracter de fim de execucao lexica na array
        con.printOutput(); //printa o resultado do analisador lexico
        
        this.vet_tokens = con.returnVetTokens(); //retorna um vetor com o valor dos tokens compilados
        this.global_counter = con.getGlobalCounterValue();// seta o valor do contador com novo valor da linha
    }
    
    public String[] getVetTokens (){
        return this.vet_tokens; 
    }
    
    
}

