/* UNIVERSIDADE CATOLICA DOM BOSCO - CAMPO GRANDE MS
DISCIPLINA DE COMPILADORES II
TRABALHO FINAL DE COMPILADORES
NOME DO PROJETO: IMPLEMENTACAO DO ANALISADOR LEXICO E SINTATICO DA LINGUAGEM LALGOL
PROFESSOR: PERICLES CHRISTIAN MORAES LOPES
ALUNO-AUTOR: MILAD ROGHANIAN
DATA DE IMPLEMENTACAO: DE 04/2019 A 02/05/2019
*/
package compilador_lex_parser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ControleLexico 
{
    ArrayList<String> lines = new ArrayList<String>();
    private String output = new String(); //todas as saidas de texto
    private Integer global_counter = 0; //contador das linhas do arquivo de codigo original
    private Integer numberCodeLines = 0;

    
    public void readtxt () 
    {
        try {
            JOptionPane.showMessageDialog(null,"ESCOLHA O TEXTO DE ENTRADA");
            JFileChooser chooserArquivo = new JFileChooser();
            chooserArquivo.showOpenDialog(null);
            FileInputStream stream = new FileInputStream(chooserArquivo.getSelectedFile().getAbsolutePath());
            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader br = new BufferedReader(reader);
            String linha = br.readLine();
            
            int count=1;
            while(linha!=null)    
            {
                lines.add(linha);
                //JOptionPane.showMessageDialog(null,lines.get(count));
                linha=br.readLine();
                count++;
            }		
            br.close();
            this.numberCodeLines = count;

        } catch (IOException e) {
            System.err.print("Erro de leitura");
        }
    }
    
    /*Faz a classificação do primeiro char do token direcionando a uma função: 
        - readIdent (Identificador) - classifier = 1
        - readSymbol (Símbolos) - classifier = 2
        - readNumbers (Números) - classifier = 3
        - readCommments (Comentários) - classifier = 4
        - readError (Erro) - classifier = 0    
    */    
    public void compileLex(int mode, int line) throws IOException{
        
        //Seta a linha a partir da qual se deseja fazer a analise
        this.setGlobalCounterValue(line);
        if (mode==0){ //da linha desejada ate a proxima
            this.output = ""; //zera a saida de texto
            classifyToken();
            this.global_counter++;
        } else if(mode==1) { //da linha desejada ate o fim do codigo
            //Exibe classifica codigo de entrada do line ate o fim
            this.output = ""; //zera a saida de texto
            while (this.global_counter<lines.size()){ 
                classifyToken ();
                this.global_counter++;
            }
        } else {
            //algo para saber que o valor do modo esta errado
        }  
        
    }
    
    //#### FUNCOES AUXILIARES ####
    
    public int getNumberCodeLines (){
        return this.numberCodeLines;
    }
    
    public void insertEndLex (){
        this.output = this.output + "{~}";
    }
    
    public int getGlobalCounterValue (){
        return this.global_counter;
    }
            
    public void setGlobalCounterValue (int position){
        this.global_counter = position;
    }
    
    public String[] returnVetTokens (){
        //cada posição do vetor de strings vai ser preenchido pelos tokens
        String vec_lex[] = this.output.split("\n"); 
        return vec_lex;
    }
    
    //#### FUNCOES DA COMPIPACAO ####
    
    public void classifyToken ()
    {
        int internal_counter=0; //contador do vetor de string da linha
        int classifier; //inteiro que indica a classificacao do token
        //String internal_line[] = lines.get(this.global_counter).split("\n"); //vetor de string que separa as palavras da linha
        int value_char; //variavel char dos caracteres da linha, inicializada com 100 para entrar no while
         
        while (internal_counter<this.lines.get(this.global_counter).length()){ //enquanto nao for fim da linha ele roda o while
            value_char = (int) this.lines.get(this.global_counter).charAt(internal_counter); //pega o valor ascii do caracter da linha
            
            //Se forem letras de 'A' a 'Z' ou 'a' a 'z'
            if ((value_char >= 65 && value_char <= 90) || (value_char >= 97 && value_char <= 122)) {
                classifier=1;
                //JOptionPane.showMessageDialog(null,"Identificador");
                internal_counter = this.readIdent(internal_counter);
            
            //Se forem letras símbolos como: ( ) * + , - / : ; > = < .
            } else if ((value_char >= 40 && value_char <= 47) || (value_char >= 58 && value_char <= 62)){
                classifier=2;
                //JOptionPane.showMessageDialog(null,"Símbolo");
                internal_counter = readSymbol (internal_counter);

            //Se forem numeros de 1 a 9
            } else if (value_char >= 48 && value_char <= 57){
                classifier=3;
                //JOptionPane.showMessageDialog(null,"Números");
                internal_counter = readNumbers(internal_counter);

            //Se for símbolos de inicio de comentario: {
            } else if (value_char == 123){
                classifier=4;
                //JOptionPane.showMessageDialog(null,"Comentário");
                internal_counter = readComments(internal_counter);//os simbolos entre as chaves de comentarios sao consumidos

            //Se for um espaco, ele eh ignorado
            } else if (value_char == 32){
                //JOptionPane.showMessageDialog(null,"Espaço");
                //Nada acontece, eh ignorado
            //Se nao for nenhum dos simbolos acima, entao eh um erro
            } else {
                classifier=0;
                //JOptionPane.showMessageDialog(null,"Erro");
                internal_counter = readError (internal_counter);
            }
            internal_counter++;
        }
    }
    
    
    //Se for identificador entra aqui
    public int readIdent (int line_counter)
    {
        //lista das palavras reservadas
        String reserved_words[] = {"program", "begin", "end", "var", "integer", "real", "procedure", "else", "read", "write", "while", "if", "do", "then"}; 
        String internal_line = this.lines.get(this.global_counter); //pega uma linha do codigo
        String final_token = "";
        char actual_char; //variavel char dos caracteres da linha, inicializada com x para entrar no while
        boolean ident = true;
        
        // Se for uma letra de 'A' a 'Z' ou 'a' a 'z' ou um numero de 0 a 9
        while (ident && line_counter<internal_line.length()){ //verifica até chegar ao fim de um identificador ou palavra reservada
            actual_char = internal_line.charAt(line_counter); //pega o proximo caracter da linha
            if ((actual_char >= 65 && actual_char <= 90) || (actual_char >= 97 && actual_char <= 122) || (actual_char >= 48 && actual_char <= 57)) {
                final_token = final_token + actual_char;
                line_counter++; //soma no contador de linhas
            } else {
                ident = false;
            }
        }
        line_counter--; //tira uma contagem feita errada no fim do while
        
        //verifica se o identificador eh uma das palavras reservadas
        for (String word : reserved_words){
            if (word.equals(final_token)){
                ident=true;
                final_token = final_token + " -- " + word;
                break;
            } else {
                ident=false;
            }
        }
        //se nao for apenas o identifica como ident
        if (!ident){
            final_token = final_token + " -- ident";
        }
        
        this.output = this.output + "l_" + Integer.toString(this.global_counter) + " - " + final_token+"\n";
        return line_counter;
    }
    
    //Se for simbolo entra aqui ################ FALTA O := <= <=
    public int readSymbol (int line_counter)
    {
        String internal_line = this.lines.get(this.global_counter); //pega uma linha do codigo
        String final_token = "" ;        
        char actual_char; //variavel char dos caracteres da linha, inicializada com x para entrar no while
        
        actual_char = internal_line.charAt(line_counter); //pega o proximo caracter da linha            
        //Se forem letras símbolos como: ( ) * + , - / : ; = > .
        if ((actual_char >= 40 && actual_char <= 47) || actual_char == 58 || actual_char == 59 || actual_char == 61 || actual_char == 62){
            final_token = final_token + actual_char;
        //Se for o simbolo de <, procura-se pelo > pois <> juntos significa o diferente na linguagem 
        } else if (actual_char == 60 && line_counter+1<internal_line.length() && internal_line.charAt(line_counter+1)==62){
            final_token = final_token + actual_char + internal_line.charAt(line_counter+1);
            line_counter++;
        } else {
            final_token = final_token + actual_char; // se nao achar, quer dizer que o < esta sozinho e eh contabilizado
        }
        final_token = final_token + " -- simbolo"; //se identificando como simbolo
        this.output = this.output + "l_" + Integer.toString(this.global_counter) + " - " + final_token+"\n"; 
        return line_counter;
    }
    
    //Se for numero entra aqui
    public int readNumbers (int line_counter)
    {
        String internal_line = this.lines.get(this.global_counter); //pega uma linha do codigo
        String final_token = "" ;        
        char actual_char; //variavel char dos caracteres da linha, inicializada com x para entrar no while
        boolean numb = true;
        boolean ident = true;
        
        while (numb && line_counter<internal_line.length()){ //verifica ate chegar ao fim de um identificador ou palavra reservada
            actual_char = internal_line.charAt(line_counter); //pega o proximo caracter da linha            
            //Se forem numeros de 1 a 9
            if ((actual_char >= 48) && (actual_char <= 57)){
                final_token = final_token + actual_char;
                line_counter++;
            //se for um simbolo de quebra de linha ou espaco
            } else if ((actual_char>=0 && actual_char<=32) || actual_char==44){
                numb=false;
            } else {
                final_token = final_token + actual_char;
                line_counter++;
                ident = false;
            }
        }
        line_counter--; //tira uma contagem a mais feita no fim do while
        
        if (ident) {
            final_token = final_token + " -- numero"; //se identificando como numero              
        } else {
            final_token = final_token + " -- erro"; //se identificando como erro pois nao e um identificador 
        }
        this.output = this.output + "l_" + Integer.toString(this.global_counter) + " - " + final_token+"\n";
        return line_counter;
    }
    
    //Se for erro entra aqui
    public int readError (int line_counter)
    {
        String internal_line = this.lines.get(this.global_counter); //pega uma linha do codigo
        char final_token = internal_line.charAt(line_counter);
        //final_token = final_token + " -- erro"; //se identificando como simbolo
        this.output = this.output + "l_" + Integer.toString(this.global_counter) + " - " + final_token + " -- erro"+"\n"; 
        return line_counter;
    }
    
    //Se for comentario entra aqui
    public int readComments (int line_counter)
    {
        String internal_line = this.lines.get(this.global_counter);
        char actual_char; //variavel char dos caracteres da linha, inicializada com x para entrar no while
        boolean com = true;
        
        while (com && line_counter<internal_line.length()){ //verifica até chegar ao fim de um identificador ou palavra reservada
            actual_char = internal_line.charAt(line_counter); //pega o proximo caracter da linha            
            //Se nao encontrar o simbolo de fim de comentario...
            if (actual_char != 125){
                line_counter++; //...consome os simbolos...
            } else {
                com=false;//...ate encontrar  
            }
        }
        
        return line_counter;
    }
    
    //Printa o codigo de entrada
    public void printImput (){       
        String stg_aux="";
        for (int count=0; count<lines.size(); count++){ 
             stg_aux = stg_aux + "l_" + Integer.toString(count) + " - " +  lines.get(count) + "\n";
        }
        JOptionPane.showMessageDialog(null,stg_aux);
    }
    
    //Printa a string de saida
    public void printOutput () throws IOException{
        JOptionPane.showMessageDialog(null,this.output);     
        File outFile = new File("output_lex.txt");
        FileWriter fw = new FileWriter(outFile);
        fw.write(this.output);
        fw.close();
    }
}

