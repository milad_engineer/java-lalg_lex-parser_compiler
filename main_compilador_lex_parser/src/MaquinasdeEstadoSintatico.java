/* UNIVERSIDADE CATOLICA DOM BOSCO - CAMPO GRANDE MS
DISCIPLINA DE COMPILADORES II
TRABALHO FINAL DE COMPILADORES
NOME DO PROJETO: IMPLEMENTACAO DO ANALISADOR LEXICO E SINTATICO DA LINGUAGEM LALGOL
PROFESSOR: PERICLES CHRISTIAN MORAES LOPES
ALUNO-AUTOR: MILAD ROGHANIAN
DATA DE IMPLEMENTACAO: DE 04/2019 A 02/05/2019
*/

package compilador_lex_parser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class MaquinasdeEstadoSintatico {
    ControleSintatico cont_sin;
    private String out_string = new String();
    String[] tokens;
    int global_counter = 0;
    static final int TOKEN = 0;
    static final int CLASS_TOKEN = 1;
    static final String END_LEX = "{~}"; //END_LEX e um caractere que sinaliza o fim da analise lexica, ele so pode ser encontrado no fim da analise
    
    //#### FUNCAO PRINCIPAL QUE INICIA A COMPILACAO DAS MAQUINAS ####
    public void initCompilation() throws IOException {
        this.cont_sin = new ControleSintatico();
        this.cont_sin.initParsing();
        
        //realiza compilacao sintatica
        initMachines(); //compila todos os tokens do vetor 
        printOutput(); //printa o resultado do analisador sintatico
    }
    
    //#### FUNCAO INICIA AS MAQUINAS DE ESTADOS ####
    public void initMachines () throws IOException{
        this.tokens = divTokens();
        this.machineProgram();
    }
    
    //#### FUNCOES AUXILIARES ####
    
    //Funcao divide o vetor de tokens recebido em um novo vetor de strings contendo em cada posicao
    //o token e sua classificacao lexica separados por @ respectivamente 
    public String[] divTokens (){
        String[] local_vet = cont_sin.getVetTokens();
        
        for (int i=0; i<local_vet.length-1; i++){
            String token = local_vet[i];
            String aux1[] = token.split(" ");
            local_vet[i] = aux1[2] + "@" + aux1[4];
        }
        return local_vet;
    }
    
    //Soma o valor anterior do contador com o da função
    public void setGlobalCounterPlusValue (){
        this.global_counter = this.global_counter + 1;
    }
    
    //Printa a string de saida
    public void printOutput () throws IOException{
        JOptionPane.showMessageDialog(null,this.out_string);     
        File outFile = new File("output_pars.txt");
        FileWriter fw = new FileWriter(outFile);
        fw.write(this.out_string);
        fw.close();
    }
    
    //#### FUNCOES DAS MAQUINAS DE ESTADOS ####
    
    //MAQUINA DE ESTADO 1 - <PROGRAMA>
    public void machineProgram() {
        String[] compare_tokens = {"program", "ident", ";", "<corpo>", ".", "erro"};
        String[] my_token;

        for (int i=0; i<=5; i++){
            my_token = tokens[this.global_counter].split("@");
            //Verificando se os tokens nao acabaram
            if (i<5 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                this.out_string = this.out_string + "\n    ANALYSIS FINISHED WITH ERROR!";
                i = 100;
            } else if (i==3 && !my_token[TOKEN].equals(compare_tokens[4])){
                machineCorpo();
            } else if (i==3 && my_token[TOKEN].equals(compare_tokens[4])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                this.setGlobalCounterPlusValue();
                i++;
            } else if (i==5 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + "\n    ANALYSIS FINISHED WITH SUCCESS!";
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[5])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[5])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                //Se nao, eh um erro de sintaxe, ou eh um simbolo
                } else {
                    //se for um simbolo
                    if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(compare_tokens[i])){
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + compare_tokens[i] + " --OK \n"; 
                    } else { //senao eh um erro de sintaxe
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
                    }
                }
                this.setGlobalCounterPlusValue();
            }
        }
    }
    
    //MAQUINA DE ESTADO 2 - <CORPO>
    public void machineCorpo() {
        String[] compare_tokens = {"<dc>", "begin", "<comandos>", "end", "erro"};
        String[] my_token;

        for (int i=0; i<=3; i++){
            my_token = tokens[this.global_counter].split("@");
            
            //Verificando se os tokens nao acabaram
            if (i<=3 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                i = 100;
            //se o primeiro token for begin
            } else if (i==0 && !my_token[TOKEN].equals(compare_tokens[1])){
                machineDc();
            //Se o primeiro token nao for begin
            } else if (i==0 && my_token[TOKEN].equals(compare_tokens[1])) { 
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                this.setGlobalCounterPlusValue();
                i++;
            } else if (i==2 && !my_token[TOKEN].equals(compare_tokens[3])){
                machineComandos();
                this.setGlobalCounterPlusValue();
            } else if (i==2 && my_token[TOKEN].equals(compare_tokens[3])) {
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                this.setGlobalCounterPlusValue();
                i++;
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[4])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[4])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                }
                this.setGlobalCounterPlusValue();
            }         
        }
    }
    
    //MAQUINA DE ESTADO 3 - <DC>
    public void machineDc() {
        //String[] compare_tokens = {"<dc_v>", "<dc_p>"};
        String[] my_token;
        my_token = tokens[this.global_counter].split("@");
        boolean var = false; 
        
        if (my_token[TOKEN].equals("var")) {
            machineDcv();
            var = true;
        }    
        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED var OR procedure \n";
        } else if (my_token[TOKEN].equals("procedure")){
            machineDcp();
            my_token = tokens[this.global_counter].split("@");
            if (my_token[TOKEN].equals("var")) {
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  EXPECTED BEFORE PROCEDURE \n";
                this.setGlobalCounterPlusValue();
            }
        } else if (my_token[CLASS_TOKEN].equals("erro")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            this.setGlobalCounterPlusValue();
        //se nao entrar nem em var nem em procedure e tiver outro token
        }  else if (!var){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED var OR procedure BUT FOUNDED " + my_token[TOKEN] +"\n";
            this.setGlobalCounterPlusValue();
        }  
    }
    
    //MAQUINA DE ESTADO 4 - <DCV>
    public void machineDcv() {
        String[] compare_tokens = {"var", "<variaveis>", ":", "<tipo_var>", ";", "<dc_v>", "erro"};
        String[] my_token;

        for (int i=0; i<=5; i++){
            my_token = tokens[this.global_counter].split("@");
            
            //Verificando se os tokens nao acabaram
            if (i<=5 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                i = 100;
            } else if (i==1){
                System.out.println("machineVariaveis()");
                machineVariaveis();
            } else if (i==3) {
                System.out.println("machineTipoVar()");
                machineTipoVar();
            } else if (i==5) {
                if (my_token[CLASS_TOKEN].equals(compare_tokens[0])){ //verifica se tem mais var
                    System.out.println("machineDcv()");
                    machineDcv();
                } //se nao tiver nao faz nada
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[6])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[6])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                //Se nao, eh um erro de sintaxe, ou eh um simbolo
                } else {
                    //se for um simbolo
                    if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(compare_tokens[i])){
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + compare_tokens[i] + " --OK \n"; 
                    } else { //senao eh um erro de sintaxe
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
                    }
                }
                this.setGlobalCounterPlusValue();
            }
        }
    }
    
    //MAQUINA DE ESTADO 5 - <TIPOVAR>
    public void machineTipoVar() {
        String[] compare_tokens = {"real", "integer", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED real OR integer \n";
        //Se a classe do token for a esperada e nao for um erro
        } else if (my_token[CLASS_TOKEN].equals(compare_tokens[0]) || my_token[CLASS_TOKEN].equals(compare_tokens[1])){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
        //Se a classe do token retornou um erro
        } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
        //Se nao, eh um erro de sintaxe
        } else {
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED real OR integer BUT FOUNDED " + my_token[TOKEN] +"\n";
        }
        this.setGlobalCounterPlusValue();
    }
    
    //MAQUINA DE ESTADO 6 - <VARIAVEIS>
    public void machineVariaveis() {
        //String[] compare_tokens = {"ident", "<mais_var>"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED ident \n";
        //Se a classe do token for um identificador
        } else if (my_token[CLASS_TOKEN].equals("ident")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
            this.setGlobalCounterPlusValue();
            machineMaisVar();
        //Se 
        } else if (my_token[CLASS_TOKEN].equals("erro")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            this.setGlobalCounterPlusValue();
            machineMaisVar();
        } //se nao for significa que nao temos mais variaveis
    }
    
    //MAQUINA DE ESTADO 7 - <MAISVAR>
    public void machineMaisVar() {
        //String[] compare_tokens = {",", "<variaveis>"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
         //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            //nada esperado
        //Se a classe do token for um simbolo, e for uma virgula quer dizer que tem mais variaveis
        } else if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(",")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
            this.setGlobalCounterPlusValue();
            machineVariaveis();
        } //se nao for significa que nao temos mais variaveis
    }
    
    //MAQUINA DE ESTADO 8 - <DCP>
    public void machineDcp() {
       String[] compare_tokens = {"procedure", "ident", "(", "<parametros>", ")", ";", "<corpo_p>", "<dc_p>", "erro"};
        String[] my_token;

        for (int i=0; i<=7; i++){
            my_token = tokens[this.global_counter].split("@");
            
            //Verificando se os tokens nao acabaram
            if (i<=7 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                i = 100;
            } else if (i==3){
                System.out.println("machineParametros()");
                machineParametros();
            } else if (i==6) {
                System.out.println("machineCorpoP()");
                machineCorpoP();
            } else if (i==7) {
                if (my_token[TOKEN].equals("procedure")){
                    System.out.println("machineDcp()");
                    machineDcp();
                }
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[6])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[6])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                //Se nao, eh um erro de sintaxe, ou eh um simbolo
                } else {
                    //se for um simbolo
                    if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(compare_tokens[i])){
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + compare_tokens[i] + " --OK \n"; 
                    } else { //senao eh um erro de sintaxe
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
                    }    
                }
                this.setGlobalCounterPlusValue();
            }
        }
    }
    
    //MAQUINA DE ESTADO 9 - <PARAMETROS>
    public void machineParametros() {
       machineListaPar();
    }
    
    //MAQUINA DE ESTADO 10 - <LISTAPAR>
    public void machineListaPar() {
        String[] compare_tokens = {"<variaveis>", ":", "<tipo_var>", "<mais_par>", "erro"};
        String[] my_token;

        for (int i=0; i<=3; i++){
            my_token = tokens[this.global_counter].split("@");
            
            //Verificando se os tokens nao acabaram
            if (i<=3 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                i = 100;
            } else if (i==0){
                machineVariaveis();
            } else if (i==2){
                machineTipoVar();
            } else if (i==3){
                machineMaisVar();
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[4])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[4])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                //Se nao, eh um erro de sintaxe
                } else {
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + " BUT FOUNDED " + my_token[TOKEN] +"\n";   
                }
                this.setGlobalCounterPlusValue();
            }
        }
    }
    
    //MAQUINA DE ESTADO 11 - <CORPOP>
    public void machineCorpoP() {
       String[] compare_tokens = {"<dc_loc>", "begin", "<comandos>", "end", ";", "erro"};
        String[] my_token;

        for (int i=0; i<=4; i++){
            my_token = tokens[this.global_counter].split("@");
            
            //Verificando se os tokens nao acabaram
            if (i<=4 && my_token[TOKEN].equals(END_LEX)){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + "\n";
                i = 100;
            //Se o primeiro token for begin
            } else if (i==0 && my_token[CLASS_TOKEN].equals(compare_tokens[1])) {
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                this.setGlobalCounterPlusValue();
                i++;
            //Se o primeiro token nao for begin
            } else if (i==0 && !my_token[CLASS_TOKEN].equals(compare_tokens[1])){
                machineDcloc();
                System.out.println("machineDcloc()");
            //Se apos begin vierem os comandos
            } else if (i==2 && !my_token[CLASS_TOKEN].equals(compare_tokens[3])) {
                machineComandos();
                System.out.println("machineComandos()");
            //Se apos begin vier o end
            } else if (i==2 && my_token[CLASS_TOKEN].equals(compare_tokens[3])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                this.setGlobalCounterPlusValue();
                i++;
            } else {
                //Se a classe do token for a esperada e nao for um erro
                if (my_token[CLASS_TOKEN].equals(compare_tokens[i]) && !my_token[CLASS_TOKEN].equals(compare_tokens[5])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                //Se a classe do token retornou um erro
                } else if (my_token[CLASS_TOKEN].equals(compare_tokens[5])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                //Se nao, eh um erro de sintaxe, ou eh um simbolo
                } else {
                    //se for um simbolo
                    if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(compare_tokens[i])){
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + compare_tokens[i] + " --OK \n"; 
                    } else { //senao eh um erro de sintaxe
                        this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[i] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
                    }
                }
                this.setGlobalCounterPlusValue();
            }         
        }
    }
    
    //MAQUINA DE ESTADO 12 - <COMANDOS>
    public void machineComandos() {
        //String[] compare_tokens = {"<cmd>",";","<comandos>"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED <cmd> \n";
        } 
        machineCmd();
        //Se a classe do token for um simbolo ;
        if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(";")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
            this.setGlobalCounterPlusValue();
            if (!my_token[CLASS_TOKEN].equals("end")){ //se o proximo token nao for um end
                machineComandos();
            }
        } else {
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED ; BUT FOUNDED " + my_token[TOKEN] +"\n";
        }
    }
    
    //MAQUINA DE ESTADO 13 - <DCLOC>
    public void machineDcloc (){
        machineDcv();
    }
    
    //MAQUINA DE ESTADO 14 - <LISTAARG>
    public void machineListaArg (){
        machineArgumentos();
    }
    
    //MAQUINA DE ESTADO 15 - <ARGUMENTOS>
    public void machineArgumentos (){
        String[] compare_tokens = {"ident", "<mais_ident>", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED ident \n";
        } else {
            if (my_token[CLASS_TOKEN].equals(compare_tokens[0])){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            //Se a classe do token retornou um erro
            } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else {
                //senao eh um erro de sintaxe
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[0] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
            }
            this.setGlobalCounterPlusValue();

            my_token = tokens[this.global_counter].split("@");    
            machineMaisIdent();
        }
    }
    
    //MAQUINA DE ESTADO 16 - <MAISIDENT>
    public void machineMaisIdent (){
        //String[] compare_tokens = {";", "<argumento>"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        
        //Se a classe do token for um simbolo, e for um ponto-virgula quer dizer que tem mais argumentos
        if (my_token[CLASS_TOKEN].equals("simbolo") && my_token[TOKEN].equals(";")){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
            this.setGlobalCounterPlusValue();
            machineArgumentos();
        } //se nao for significa que nao temos mais argumentos
    }
    
    //MAQUINA DE ESTADO 17 - <PFALSA>
    public void machinePfalsa (){
        String[] compare_tokens = {"else", "<cmd>", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){  
            if (my_token[TOKEN].equals(compare_tokens[0])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
                machineCmd();
            //Se a classe do token retornou um erro
            } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else {
                //senao eh um erro de sintaxe
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[0] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
            }
            this.setGlobalCounterPlusValue(); 
        }
    }
    
    //MAQUINA DE ESTADO 18 - <CMD>
    public void machineCmd (){
        String[] my_token, my_token2;

        my_token = tokens[this.global_counter].split("@");
        my_token2 = tokens[this.global_counter+1].split("@");
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED <cmd> \n";
        } else { 
            
            if (my_token[TOKEN].equals("read")){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                machineVariaveis();
            } else if (my_token[TOKEN].equals("write")){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                machineVariaveis();
            } else if (my_token[TOKEN].equals("while")){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                machineCondicao();
                if (my_token[TOKEN].equals("do")){ //se o token for o desejado
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                } else if (my_token[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                } else { //senao eh um erro de sintaxe
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED do BUT FOUNDED " + my_token[TOKEN] +"\n";
                }
                machineCmd();
                machinePfalsa();
            } else if (my_token[TOKEN].equals("if")){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                machineCondicao();
                if (my_token[TOKEN].equals("then")){ //se o token for o desejado
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                } else if (my_token[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                } else { //senao eh um erro de sintaxe
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED then BUT FOUNDED " + my_token[TOKEN] +"\n";
                }
                machineCmd();
                machinePfalsa();
            } else if (my_token[TOKEN].equals("ident")){
                if (my_token2[TOKEN].equals(":=")){ //se o token seguinte for o desejado
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                    machineExpressao();
                } else if (my_token2[TOKEN].equals(";")) {
                    machineListaArg();
                } else if (my_token2[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                } else { //senao eh um erro de sintaxe
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED := OR ; BUT FOUNDED " + my_token[TOKEN] +"\n";
                }
            } else if (my_token[TOKEN].equals("begin")){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                machineComandos();
                if (my_token[TOKEN].equals("end")){ //se o token for o desejado
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                } else if (my_token[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                } else { //senao eh um erro de sintaxe
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED end BUT FOUNDED " + my_token[TOKEN] +"\n";
                }
            } else if (my_token[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else { //senao eh um erro de sintaxe
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED cmd comands BUT FOUNDED " + my_token[TOKEN] +"\n";
            }
            this.setGlobalCounterPlusValue();
        }
    }
    
    //MAQUINA DE ESTADO 19 - <CONDICAO>
    public void machineCondicao(){
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            machineExpressao();
            machineRelacao();
            machineExpressao();
        }
       
    }
    
    public void machineRelacao(){
        String[] compare_tokens = {"=", "<>", ">=", "<=", ">", "<", "erro"};
        String[] my_token;
        boolean state = false;

        my_token = tokens[this.global_counter].split("@");
        
        //Verificando se os tokens nao acabaram
        if (my_token[TOKEN].equals(END_LEX)){
            this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED <Expressao> \n";
        } else {
            for (int i=0; i<=5; i++){
                if (my_token[TOKEN].equals(compare_tokens[i])){
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n";
                    i=10;
                    state=true;
                } 
            }
            if (!state){
                if (my_token[CLASS_TOKEN].equals("erro")){ //se o lexico retornar um erro
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
                } else { //senao eh um erro de sintaxe
                    this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED simbol BUT FOUNDED " + my_token[TOKEN] +"\n";
                }
            }
            this.setGlobalCounterPlusValue();
        } 
            

    }
    
    public void machineExpressao(){
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            machineTermo();
            machineOutrosTermos();
        }
    }
    
    public void machineOpun (){
        String[] compare_tokens = {"+", "-", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            // se achou um +
            if (my_token[TOKEN].equals(compare_tokens[0])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            // se achou um -
            } else if (my_token[TOKEN].equals(compare_tokens[1])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            //Se a classe do token retornou um erro
            } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else {
                
            }
            this.setGlobalCounterPlusValue(); 
        }
    }
    
    public void machineOutrosTermos (){
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            machineOpad();
            machineTermo();
            machineTermo();
            machineOutrosTermos();
        }
    }
    
    public void machineOpad (){
        String[] compare_tokens = {"+", "-", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            // se achou um +
            if (my_token[TOKEN].equals(compare_tokens[0])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            // se achou um -
            } else if (my_token[TOKEN].equals(compare_tokens[1])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            //Se a classe do token retornou um erro
            } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else {
                //senao eh um erro de sintaxe
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--EXPECTED " + compare_tokens[0] + " BUT FOUNDED " + my_token[TOKEN] +"\n";
            }
            this.setGlobalCounterPlusValue(); 
        }
    } 
    
    public void machineTermo (){
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            machineOpun();
            machineFator();
            machineMaisFatores();
        }
    }
    
    public void machineMaisFatores (){
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            machineOpmul();
            machineFator();
            machineMaisFatores();
        }
    }
    
    public void machineOpmul (){
        String[] compare_tokens = {"*", "/", "erro"};
        String[] my_token;

        my_token = tokens[this.global_counter].split("@");
        //Verificando se os tokens nao acabaram
        if (!my_token[TOKEN].equals(END_LEX)){
            // se achou um +
            if (my_token[TOKEN].equals(compare_tokens[0])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            // se achou um -
            } else if (my_token[TOKEN].equals(compare_tokens[1])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + "  " + my_token[TOKEN] + " --OK \n"; 
            //Se a classe do token retornou um erro
            } else if (my_token[CLASS_TOKEN].equals(compare_tokens[2])){
                this.out_string = this.out_string + Integer.toString(this.global_counter) + " --ERRO--TOKEN  " + my_token[TOKEN] + "  NOT EXIST \n";
            } else {
            }
            this.setGlobalCounterPlusValue(); 
        }
    }
    
    public void machineFator (){
        machineFator();
    }
}
