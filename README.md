========================
BUILD OUTPUT DESCRIPTION
========================

When you build an Java application project that has a main class, the IDE
automatically copies all of the JAR
files on the projects classpath to your projects dist/lib folder. The IDE
also adds each of the JAR files to the Class-Path element in the application
JAR files manifest file (MANIFEST.MF).

To run the project from the command line, go to the dist folder and
type the following:

java -jar "Compilador_lex_parser.jar" 

To distribute this project, zip up the dist folder (including the lib folder)
and distribute the ZIP file.

Notes:

* If two JAR files on the project classpath have the same name, only the first
JAR file is copied to the lib folder.
* Only JAR files are copied to the lib folder.
If the classpath contains other types of files or folders, these files (folders)
are not copied.
* If a library on the projects classpath also has a Class-Path element
specified in the manifest,the content of the Class-Path element has to be on
the projects runtime path.
* To set a main class in a standard Java project, right-click the project node
in the Projects window and choose Properties. Then click Run and enter the
class name in the Main Class field. Alternatively, you can manually type the
class name in the manifest Main-Class element.


UNIVERSIDADE CATOLICA DOM BOSCO - CAMPO GRANDE MS
DISCIPLINA DE COMPILADORES II
TRABALHO FINAL DE COMPILADORES
NOME DO PROJETO: IMPLEMENTACAO DO ANALISADOR LEXICO E SINTATICO DA LINGUAGEM LALGOL
PROFESSOR: PERICLES CHRISTIAN MORAES LOPES
ALUNO-AUTOR: MILAD ROGHANIAN
DATA DE IMPLEMENTACAO: DE 04/2019 A 02/05/2019

1. Na pasta do projeto (main_compiler_lex_parser) existem 3 subpastas, src, dist e inputs. 
2. Na pasta dist encontra-se o .jar do projeto executavel em java. Basta ter a última versao do compilador java instalado em sua maquina para executa-lo
Mais instrucoes na execucao do jar e do compilador java estao no inicio deste documento
3. Na pasta src encontram-se os codigos fonte do projeto escritos em java. Podem ser abertos por qualquer leitor de texto ou IDE java para execucao. 
Basta executar a main do projeto para compilar a partir do codigo fonte. 
4. Em cada execucao do programa ele pedira um codigo lalg .txt de entrada, basta selecionar para que este seja executado no compilador lexico
e sintatico do lalg. Existem 3 exemplos de codigo lalg na pasta inputs. Tambem, em cada execucao do programa, sera gerado 2 .txt de saida com 
o resultado da execao lexica e sintatica, com os nome de output_lex.txt e output_pars.txt respectivamente
